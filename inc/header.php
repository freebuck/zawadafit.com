<?php 
session_start();
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>ZAWADAFIT | Homepage</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta name="keywords" content="..." />
	<meta name="author" content="Hashemi Rafsan, Azharul Islam , HB Shifat" />

  

  <!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">

	<!-- Google Webfonts -->
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Monoton" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="http://localhost:36/zawad/inc/css/animate.css">
	<!-- Icon Fonts-->
	<link rel="stylesheet" href="http://localhost:36/zawad/inc/css/icomoon.css">
	<link rel="stylesheet" type="http://localhost:36/zawad/text/css" href="inc/fonts/font-awesome-4.6.3/css/font-awesome.min.css">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="http://localhost:36/zawad/inc/css/owl.carousel.min.css">
	<link rel="stylesheet" href="http://localhost:36/zawad/inc/css/owl.theme.default.min.css">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="http://localhost:36/zawad/inc/css/magnific-popup.css">
	<!-- Theme Style -->
	<link rel="stylesheet" href="http://localhost:36/zawad/inc/css/style.css">
	<!-- Modernizr JS -->
	<script src="inc/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
		
	<header id="fh5co-header" role="banner">
		<div id="fh5co-footer">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-6">
						<div class="fh5co-footer-widget">
							<ul class="fh5co-social">
								<li><a href="#"><i class="icon-facebook"></i></a></li>
								<li><a href="#"><i class="icon-twitter"></i></a></li>
								<li><a href="#"><i class="icon-instagram"></i></a></li>
								<li><a href="#"><i class="icon-linkedin"></i></a></li>
								<li><a href="#"><i class="icon-youtube"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 text-right login">
					<?php 
					if(!empty($_SESSION['logged']) == "logged"){
						echo 'Hi ,' . $_SESSION['firstname'].' |';
						echo '<a href="http://localhost:36/zawad/logout.php">Logout</a>';
					} else {
						echo '<a href="http://localhost:36/zawad/member/login.php" style="color:white; padding:0px 10px;">Login</a>|';
						echo '<a href="#" style="padding:0px 0px 0px 10px;">Sign Up Now</a>';
					}
					
					?>
					</div>
				</div>			
			</div>
		</div>
		<nav class="navbar navbar-default" role="navigation">
			<div class="container">
				<div class="row">
					<div class="navbar-header"> 
					<!-- Mobile Toggle Menu Button -->
					<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#fh5co-navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
					<a class="navbar-brand" href="index.html">ZAWADAFIT</a>
					
					</div>
					<div id="fh5co-navbar" class="navbar-collapse collapse">
						<ul class="nav navbar-nav navbar-right">
							<li class=" dropdown">
								<a href="" class="dropdown-toggle" data-toggle="dropdown">
									<span>WorkOut <i class="fa fa-angle-down"></i>
										<span class="border"></span>
									</span>
								</a>

								<ul class="dropdown-menu">
									<li><a href="#">ITEM 1</a></li>
									<li><a href="#">ITEM 2</a></li>
									<li><a href="#">ITEM 3</a></li> 
									<li><a href="#">ITEM 4</a></li>
									<li><a href="#">ITEM 5</a></li>
									<li><a href="#">ITEM 6</a></li> 
						        </ul>


							</li>
							<li class="dropdown">
								<a href="" class="dropdown-toggle" data-toggle="dropdown">
									<span>Interactive Body Map <i class="fa fa-angle-down"></i>
										<span class="border"></span>
									</span>
								</a>

								<ul class="dropdown-menu">
									<li><a href="#">ITEM 1</a></li>
									<li><a href="#">ITEM 2</a></li>
									<li><a href="#">ITEM 3</a></li> 
									<li><a href="#">ITEM 4</a></li>
									<li><a href="#">ITEM 5</a></li>
									<li><a href="#">ITEM 6</a></li> 
						        </ul>
							</li>


							<li class="dropdown">
								<a href="" class="dropdown-toggle" data-toggle="dropdown">
									<span>Nutrition <i class="fa fa-angle-down"></i>
										<span class="border"></span>
									</span>

								</a>

								<ul class="dropdown-menu">
									<li><a href="http://localhost:36/zawad/Nutrition/cook-book/">zawadafit cook books</a></li>
									<li><a href="">Nutrition & Diet</a></li>
						        </ul>
							</li>
							<li><a href="online-coaching/index.php"><span>Online Couching  <span class="border"></span></span></a></li>
							<li><a href="blog/index.php"><span>Blog  <span class="border"></span></span></a></li>
							<li><a href="testimonial/index.php"><span>Testimonial  <span class="border"></span></span></a></li>
							<li><a href="Contact/index.php"><span>Contact<span class="border"></span></span></a></li>
						</ul>
					</div>
				</div>
			</div>
		</nav>
	</header>
	<!-- END .header -->