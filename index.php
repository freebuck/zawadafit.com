<?php 
include 'inc/header.php';
include 'function/database.php';
?>

	<div class="fh5co-slider">
		<div class="owl-carousel owl-carousel-fullwidth">
		    <div class="item" style="background-image:url(inc/images/1.jpg)">
		    	<div class="fh5co-overlay"></div>
		    	<div class="container">
		    		<div class="row">
		    			<div class="col-md-8 col-md-offset-2">
			    			<div class="fh5co-owl-text-wrap">
						    	<div class="fh5co-owl-text text-center to-animate">
						    		<h1 class="fh5co-lead">LETS GET BETTER TOGETHER</h1>
						    	</div>
						    </div>
					    </div>
		    		</div>
		    	</div>
		    </div>
		    <div class="item" style="background-image:url(inc/images/2.jpg)">
		    	<div class="fh5co-overlay"></div>
		    	<div class="container">
		    		<div class="row">
		    			<div class="col-md-8 col-md-offset-2">
			    			<div class="fh5co-owl-text-wrap">
						    	<div class="fh5co-owl-text text-center to-animate int">
						    		<h1 class="fh5co-lead">WE ARE EXPERT TO DO THIS</h1>
						    	</div>
						    </div>
					    </div>
		    		</div>
		    	</div>
		    </div>
		    <div class="item" style="background-image:url(inc/images/3.jpg)">
		    	<div class="fh5co-overlay"></div>
		    	<div class="container">
		    		<div class="row">
		    			<div class="col-md-8 col-md-offset-2">
			    			<div class="fh5co-owl-text-wrap">
						    	<div class="fh5co-owl-text text-center to-animate">
						    		<h1 class="fh5co-lead">TRUST is EVERYTHING FOR US</h1>
						    	</div>
						    </div>
					    </div>
		    		</div>
		    	</div>
		    </div>
		    <div class="item" style="background-image:url(inc/images/2.jpg)">
		    	<div class="fh5co-overlay"></div>
		    	<div class="container">
		    		<div class="row">
		    			<div class="col-md-8 col-md-offset-2">
			    			<div class="fh5co-owl-text-wrap">
						    	<div class="fh5co-owl-text text-center to-animate">
						    		<h1 class="fh5co-lead">LIFE IS BEAUTIFUL</h1>
						    	</div>
						    </div>
					    </div>
		    		</div>
		    	</div>
		    </div>
		</div>
	</div>	

	<!--ad  section-->
	<div class="fh5co-spacer fh5co-spacer-lg"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-4 text-center">
					<h3>JOIN TEAM BMFIT</h3>
					<img src="images/img1.png" class="img-responsive">
				</div>
				<div class="col-md-4 text-center"> 
					<h3>GET SHREDDED</h3>
					<img src="images/img2.png" class="img-responsive">
				</div>
				<div class="col-md-4 text-center">
					<h3>BODY BUILDING</h3>
					<img src="images/img3.png" class="img-responsive">
				</div>
			</div>
		</div>
	</div>


	<div id="fh5co-main">
		<div class="fh5co-spacer fh5co-spacer-lg"></div>		
		<!-- Products -->
		<div class="container" id="fh5co-products">
			<div class="col-md-7 col-sm-12">
				<div class="row text-left">
					<div class="col-md-12">
						<h1 class="fh5co-section-lead text-center"><span style="color:#FBB040;">FEATURED</span> NEWS</h1>
					</div>
				</div>


				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="col-md-6 col-sm-6 col-xs-6 col-xxs-12 fh5co-mb30">
							<div class="fh5co-product">
								<img src="inc/images/slide_1.jpg" alt="v" class="img-responsive to-animate">
								<h4>Lorem ipsum</h4>
								<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
								<p><a href="#">Read more</a></p>
							</div>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6 col-xxs-12 fh5co-mb30">
							<div class="fh5co-product">
								<img src="inc/images/slide_2.jpg" alt="v" class="img-responsive to-animate">
								<h4>Architecto nihil</h4>
								<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
								<p><a href="#">Read more</a></p>
							</div>
						</div>
						<div class="visible-sm-block visible-xs-block clearfix"></div>
						<div class="col-md-6 col-sm-6 col-xs-6 col-xxs-12 fh5co-mb30">
							<div class="fh5co-product">
								<img src="inc/images/slide_3.jpg" alt="" class="img-responsive to-animate">
								<h4>Animi earum</h4>
								<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
								<p><a href="#">Read more</a></p>
							</div>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6 col-xxs-12 fh5co-mb30">
							<div class="fh5co-product">
								<img src="inc/images/slide_4.jpg" alt="" class="img-responsive to-animate">
								<h4>Recusandae iste</h4>
								<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
								<p><a href="#">Read more</a></p>
							</div>
						</div>
					</div>		
				</div>



				<div class="row">
					<div class="col-md-12">
						<h1 class="fh5co-section-lead text-center"><span style="color:#FBB040;">TEAM BEFIT</span> BLOG</h1>
					</div>
					<div class="col-sm-12 col-md-12 blog-sec">
						<div class="col-md-4">
							<img src="inc/images/blog1.png" alt="FREEHTML5.co Free HTML5 Template Bootstrap" class="img-responsive to-animate">
						</div>
						<div class="col-md-6 blog_detils">
							<h4 class="blog_header">Recusandae iste</h4>
							<p class="blog_info">Far far away, behind the word mountains.Far far away, behind the word </p>
							<p><a href="#" class="blog_btn">Read more</a></p>
						</div>
					</div>

					<div class="col-sm-12 col-md-12">
						<div class="col-md-4">
							<img src="inc/images/blog2.png" alt="FREEHTML5.co Free HTML5 Template Bootstrap" class="img-responsive to-animate">
						</div>
						<div class="col-md-6 blog_detils">
							<h4 class="blog_header">Recusandae iste</h4>
							<p class="blog_info">Far far away, behind the word mountains.Far far away, behind the word </p>
							<p><a href="#" class="blog_btn">Read more</a></p>
						</div>
					</div>
				</div>

			</div>



			<!--video section-->
			<div class="col-md-5">
				<!--search bar-->
				<div class="col-md-12 col-sm-12">
					<form id="subscription-form" action="#" method="post" >
						<div class="form-group">
							<input type="text" name="TEXT" placeholder="Search Videos" class="form-control form-error search input">
							<button class="btn btn-standard btn-main btn-medium search input-btn" type="submit"><i class="fa fa-search"></i></button>
						</div>
					</form>
				</div>
				<div class="col-md-12 col-sm-12 video-grp" data-video="1">
					<img src="inc/images/vid1.png" alt="video" class="img-responsive to-animate vid-target">
				</div>
				<div class="col-md-12 col-sm-12 video-grp" data-video="2">
					<img src="inc/images/vid2.png" alt="video" class="img-responsive to-animate">
				</div>
				<div class="col-md-12 col-sm-12 video-grp" data-video="3">
					<img src="inc/images/vid3.png" alt="video" class="img-responsive to-animate">
				</div>
				<div class="col-md-12 col-sm-12 video-grp" data-video="4">
					<img src="inc/images/vid4.png" alt="video" class="img-responsive to-animate">
				</div>
				<div class="col-md-12 col-sm-12 video-grp" data-video="5">
					<img src="inc/images/vid5.png" alt="video" class="img-responsive to-animate">
				</div>

			</div>
			
		</div>
	</div>

	<!--member-->
	<div class="fh5co-spacer fh5co-spacer-lg"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-12 ">
				<h2 class="text-left top-line" style="font-style:italic;">MEMBERS <span style="font-weight:bold;">TESTIMONIALS</span></h2>
				<a href="#" class="view-link">View All</a>

				<div class="row">
					<div class="col-md-6 ">
						<div class="member-large-img">
							<img src="inc/images/mem1.png" alt="img-1" data-large="1" class="member-img img-responsive" >
							<img src="inc/images/mem2.png" alt="img-1" data-large="2" class="member-img img-responsive" >
							<img src="inc/images/mem3.png" alt="img-1" data-large="3" class="member-img img-responsive" >
							<img src="inc/images/mem4.png" alt="img-1" data-large="4" class="member-img img-responsive" >
							<img src="inc/images/mem5.png" alt="img-1" data-large="5" class="member-img img-responsive" >
							<img src="inc/images/mem2.png" alt="img-1" data-large="6" class="member-img img-responsive">
							<img src="inc/images/mem1.png" alt="img-1" data-large="7" class="member-img img-responsive" >
							<img src="inc/images/mem2.png" alt="img-1" data-large="8" class="member-img img-responsive" >
							<img src="inc/images/mem3.png" alt="img-1" data-large="9" class="member-img img-responsive" >
							<img src="inc/images/mem4.png" alt="img-1" data-large="10" class="member-img img-responsive" >
							<img src="inc/images/mem5.png" alt="img-1" data-large="11" class="member-img img-responsive" >
							<img src="inc/images/mem2.png" alt="img-1" data-large="12" class="member-img img-responsive">
						</div>
					</div>
					<div class="col-md-6 member_overview">
						<div class="col-md-12">
							<div class="member-comment" data-comment="1">
								This is the only four month earlyer. now you can see change .
								so hard work.This is the only four month earlyer. now you can see change .
								so hard work.
								 <a href="#">[Read More]</a>
								 <p>-ROFF LAGAN</p>
							</div>
							<div class="member-comment" data-comment="2">
								This is the only four month earlyer. now you can see change .
								so hard work.This is the only four month earlyer. now you can see change .
								so hard work.
								 <a href="#">[Read More]</a>
								 <p>-Staff LAGAN</p>
							</div>
							<div class="member-comment" data-comment="3">
								This is the only four month earlyer. now you can see change .
								so hard work.This is the only four month earlyer. now you can see change .
								so hard work.
								 <a href="#">[Read More]</a>
								 <p>-SIMP LAGAN</p>
							</div>
							<div class="member-comment" data-comment="4">
								This is the only four month earlyer. now you can see change .
								so hard work.This is the only four month earlyer. now you can see change .
								so hard work.
								 <a href="#">[Read More]</a>
								 <p>-NAMI LAGAN</p>
							</div>
							<div class="member-comment" data-comment="5">
								This is the only four month earlyer. now you can see change .
								so hard work.This is the only four month earlyer. now you can see change .
								so hard work.
								 <a href="#">[Read More]</a>
								 <p>-DOTH LAGAN</p>
							</div>
							<div class="member-comment" data-comment="6">
								This is the only four month earlyer. now you can see change .
								so hard work.This is the only four month earlyer. now you can see change .
								so hard work.
								 <a href="#">[Read More]</a>
								 <p>-LOHA LAGAN</p>
							</div>
							<div class="member-comment" data-comment="7">
								This is the only four month earlyer. now you can see change .
								so hard work.This is the only four month earlyer. now you can see change .
								so hard work.
								 <a href="#">[Read More]</a>
								 <p>-SARA LAGAN</p>
							</div>
							<div class="member-comment" data-comment="8">
								This is the only four month earlyer. now you can see change .
								so hard work.This is the only four month earlyer. now you can see change .
								so hard work.
								 <a href="#">[Read More]</a>
								 <p>-TENT LAGAN</p>
							</div>
							<div class="member-comment" data-comment="9">
								This is the only four month earlyer. now you can see change .
								so hard work.This is the only four month earlyer. now you can see change .
								so hard work.
								 <a href="#">[Read More]</a>
								 <p>-XXX LAGAN</p>
							</div>
							<div class="member-comment" data-comment="10">
								This is the only four month earlyer. now you can see change .
								so hard work.This is the only four month earlyer. now you can see change .
								so hard work.
								 <a href="#">[Read More]</a>
								 <p>-HUTG LAGAN</p>
							</div>
							<div class="member-comment" data-comment="11">
								This is the only four month earlyer. now you can see change .
								so hard work.This is the only four month earlyer. now you can see change .
								so hard work.
								 <a href="#">[Read More]</a>
								 <p>-POTH LAGAN</p>
							</div>
							<div class="member-comment" data-comment="12">
								This is the only four month earlyer. now you can see change .
								so hard work.This is the only four month earlyer. now you can see change .
								so hard work.
								 <a href="#">[Read More]</a>
								 <p>-MALO LAGAN</p>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-2 col-sm-4 col-xs-6 mem-control">
								<img src="inc/images/sm1.png" class="img-responsive small-mem" alt="member img" data-small="1">
							</div>
							<div class="col-md-2 col-sm-4 col-xs-6 mem-control">
								<img src="inc/images/sm2.png" class="img-responsive small-mem" alt="member img" data-small="2">
							</div>
							<div class="col-md-2 col-sm-4 col-xs-6 mem-control">
								<img src="inc/images/sm3.png" class="img-responsive small-mem" alt="member img" data-small="3">
							</div> 
							<div class="col-md-2 col-sm-4 col-xs-6 mem-control">
								<img src="inc/images/sm4.png" class="img-responsive small-mem" alt="member img" data-small="4">
							</div>
							<div class="col-md-2 col-sm-4 col-xs-6 mem-control">
								<img src="inc/images/sm5.png" class="img-responsive small-mem" alt="member img" data-small="5">
							</div>
							<div class="col-md-2 col-sm-4 col-xs-6 mem-control">
								<img src="inc/images/sm6.png" class="img-responsive small-mem" alt="member img" data-small="6">
							</div>
							<div class="col-md-2 col-sm-4 col-xs-6 mem-control">
								<img src="inc/images/sm7.png" class="img-responsive small-mem" alt="member img" data-small="7">
							</div>
							<div class="col-md-2 col-sm-4 col-xs-6 mem-control">
								<img src="inc/images/sm8.png" class="img-responsive small-mem" alt="member img" data-small="8">
							</div>
							<div class="col-md-2 col-sm-4 col-xs-6 mem-control">
								<img src="images/sm9.png" class="img-responsive small-mem" alt="member img" data-small="9">
							</div>
							<div class="col-md-2 col-sm-4 col-xs-6 mem-control">
								<img src="inc/images/sm10.png" class="img-responsive small-mem" alt="member img" data-small="10">
							</div>
							<div class="col-md-2 col-sm-4 col-xs-6 mem-control">
								<img src="inc/images/sm11.png" class="img-responsive small-mem" alt="member img" data-small="11">
							</div>
							<div class="col-md-2 col-sm-4 col-xs-6 mem-control">
								<img src="inc/images/sm12.png" class="img-responsive small-mem" alt="member img" data-small="12">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="fh5co-spacer fh5co-spacer-lg"></div>


<?php 

include 'inc/footer.php';

?>